package org.gradle.rabbit;

import java.io.IOException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Consumer {	
	//-- Parametros


	
	public static String HOST = "localhost";
	public static int PORT = 5672; //-- AMQP
	public static String VHOST = "arl.vh";
	public static String EXCHANGE = "arl.gestionafilinea.afiliacion";
	public static String ROUTING_KEY = "arl.gestionafilinea.afiliacion.solicitud";
	public static String USER = "arl.gestionafilinea.usr";
    public static String PASSWD = "GFDnWWu0gp7x1eMyKE9y";
    public static String QUEUE_NAME = "arl.gestionafilinea.afiliacion.solicitud";
	
	//-- Enviar mensaje
	public void sendMessage() throws Exception{		
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		factory.setVirtualHost(VHOST);
		factory.setUsername(USER);
		factory.setPassword(PASSWD);	
		Connection connection = factory.newConnection();
		final Channel channel = connection.createChannel();		
		DefaultConsumer consumer = new DefaultConsumer(channel) {
	      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
	        String message = new String(body, "UTF-8");
	        System.out.println(" [x] Mensaje recibido: '" + message + "'");	        
	      }
	    };
		channel.basicConsume(QUEUE_NAME, true, consumer);				
		Thread.sleep(15000); //-- Esperar para recibir mensajes - Para Efectos de la Prueba 		
		channel.close();
	    connection.close();	    
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		new Consumer().sendMessage();
	}

}
