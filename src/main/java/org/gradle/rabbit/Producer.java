package org.gradle.rabbit;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


/**
 * Ejemplo de productor de mensajes
 */
public class Producer {
	/**
	 * Parametros 
	 */
	
	
	public static String HOST = "localhost";
	public static int PORT = 5672; //-- AMQP
	public static String VHOST = "arl.vh";
	public static String EXCHANGE = "arl.gestionafilinea.afiliacion";
	public static String ROUTING_KEY = "arl.gestionafilinea.afiliacion.solicitud";
	public static String USER = "arl.gestionafilinea.usr";
    public static String PASSWD = "GFDnWWu0gp7x1eMyKE9y";
		/**
	 * Enviar el mensaje
	 */
	public void sendMessage() throws Exception{		
		
		
		ConnectionFactory factory = new ConnectionFactory();
		factory.setVirtualHost(VHOST);
		factory.setHost(HOST);
		factory.setUsername(USER);
		factory.setPassword(PASSWD);
		
		/**/
		int peticiones = 1;
		
		
		/**/
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		for (int i = 1; i <= peticiones; i++) {
 			String mensaje = "mensaje"; 
			System.out.println(mensaje);
			channel.basicPublish(EXCHANGE, ROUTING_KEY, null, mensaje.getBytes("UTF-8"));
		}
		channel.close();
	    connection.close();	    
	}
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		new Producer().sendMessage();
	}
}
