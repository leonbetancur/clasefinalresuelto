package org.gradle.akka;

import akka.actor.AbstractActor;
import akka.actor.AbstractActor.Receive;

public class Actor2 extends AbstractActor {

	private void saludar(String nombre) {
		System.out.println("Saludo desde actor 2 a "+nombre);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Terminando proceso desde actor 2 a "+nombre);
	}
	
	private void numero(int a) {
		System.out.println("El n�mero enviado fue "+a);
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder().matchEquals(345, this::numero)
							   .match(String.class, this::saludar)
				               .build();
	}

}
