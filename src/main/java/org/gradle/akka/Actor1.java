package org.gradle.akka;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public class Actor1 extends AbstractActor {

	private void saludar(String nombre) {
		
		ActorRef actorHijo = getContext().actorOf(Props.create(Actor2.class));
		System.out.println("Saludo desde actor 1 a "+nombre);
		actorHijo.tell(nombre, actorHijo.noSender());
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Terminando proceso desde actor 1 a "+nombre);
	}
	
	private void numero(int a) {
		System.out.println("El n�mero enviado fue "+a);
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder().matchEquals(345, this::numero)
							   .match(String.class, this::saludar)
				               .build();
	}

}
