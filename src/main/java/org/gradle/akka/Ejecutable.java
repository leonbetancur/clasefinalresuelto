package org.gradle.akka;


import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.PatternsCS;
import akka.util.Timeout;
import scala.concurrent.duration.Duration;

public class Ejecutable {
	public static void main(String...args) {
		
		System.out.println("Inicio Sistema");
		
		ActorSystem sistema = ActorSystem.create();
		ActorRef refereciaActor1 = sistema.actorOf(Props.create(Actor1.class));
		ActorRef refereciaActor2 = sistema.actorOf(Props.create(Actor1.class));
		/*ActorRef refereciaActor3 = sistema.actorOf(Props.create(Actor1.class));
		ActorRef refereciaActor4 = sistema.actorOf(Props.create(Actor1.class));*/
		
		refereciaActor1.tell("Leon", refereciaActor1.noSender());
		CompletableFuture<Object> futuroCreaPoliza = PatternsCS.ask(refereciaActor1, "Leon", new Timeout(Duration.create(1000, "seconds"))).toCompletableFuture();
		try {
			futuroCreaPoliza.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //CompletableFuture<Object> futuroCreaPoliza =  ask(crearPolizaActor, formulario, new Timeout(Duration.create(TimeOut.TIMEOUT_CREACION_POLIZA, "seconds"))).toCompletableFuture();

		refereciaActor2.tell("Andres", refereciaActor1.noSender());
		/*refereciaActor3.tell("BEtancur", refereciaActor1.noSender());
		refereciaActor4.tell("Herrera", refereciaActor1.noSender());
		refereciaActor3.tell("Osorio", refereciaActor1.noSender());*/
		
		refereciaActor1.tell(345, refereciaActor1.noSender());
		
		
		
	}

}
