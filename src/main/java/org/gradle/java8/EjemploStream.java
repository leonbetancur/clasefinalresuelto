package org.gradle.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EjemploStream {

	public Double calculoNormal(List<Double> listaProductos) {
		Double total = Double.valueOf(0);
		for(Double producto:listaProductos) {
			if(producto>= 100) {
				total = total + (producto*16/100); 
			}
		}
		return total ;
	}
	
	public static void main(String...args) {
		List<Double> listaProductos = new ArrayList<>();
		listaProductos.add(Double.valueOf(50));
		listaProductos.add(Double.valueOf(100));
		listaProductos.add(Double.valueOf(200));
		listaProductos.add(Double.valueOf(100));
		
		EjemploStream ejemplo = new EjemploStream();
		
		
		System.out.println("Total Normal: "+ejemplo.calculoNormal(listaProductos));
		
		System.out.println("Total Stream: "+listaProductos.stream().filter(numero->numero>=100)
																   .mapToDouble(numero->numero*16/100)
																   .sum()
		                  );
		
		
		Optional<Double> primerElemento = listaProductos.stream().findFirst();
		System.out.println("Primer Elemento: "+primerElemento);
		
		Optional<Double> elementoOpcional = Optional.ofNullable(null);
		System.out.println("Elemento Opcional: "+elementoOpcional.orElse(Double.valueOf(0)));

		System.out.println("Distinct: "+listaProductos.stream().distinct().collect(Collectors.toList()));
		
		System.out.println("Filtros : "+listaProductos.stream().filter(numero->numero<100).collect(Collectors.toList()));
		
		System.out.println("cumplen condici�n:"+listaProductos.stream().allMatch(numero->numero>0));
		
		System.out.println("Doble: "+listaProductos.stream().map(a->a*2).collect(Collectors.toList()) );
		
		
		listaProductos.stream().forEach(elemento->System.out.println("El n�mero es:"+elemento));
		
		System.out.println("Sumatoria : "+listaProductos.stream().reduce(Double.valueOf(0), (x,y)->(x+y)));
		
	}
}
